package com.hackerrank.customermicroservice;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.customermicroservice.model.Customer;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@WebAppConfiguration
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerTest {

    @Autowired
    private WebApplicationContext ctx;
    private Customer testCustomer;
    private MockMvc mvc;

    @Before
    public void setup()
    {
        mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void test1_getCustomerTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/customer/1002").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void test1a_getCustomerInvalidTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/customer/0").accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }

    @Test
    public void test2_getAllCustomerTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/customer").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$[0].customerId").isNumber())
                .andExpect(jsonPath("$[0].customerName").isString())
                .andExpect(jsonPath("$[0].contactNumber").isNumber())
                .andExpect(jsonPath("$[0].address").isString())
                .andExpect(jsonPath("$[0].gender").isString());
    }

    @Test
    public void test3_createCustomerTest() throws Exception
    {

        testCustomer = new Customer(1005,"Thanos", Long.parseLong("5555555555"), "5 Street Lane", "Male");
        System.out.println("Display " + testCustomer.getCustomerId());
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/customer")
                    .content(toJson(testCustomer))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("$.customerId").isNumber())
                    .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/customer/"+json.get("customerId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.customerId").value((json.get("customerId"))))
                .andExpect(jsonPath("$.customerName").value("Thanos"));

    }

    @Test
    public void test3a_createCustomerBadRequestTest() throws Exception
    {

        testCustomer = new Customer(1002,"Thanos", Long.parseLong("5555555555"), "5 Street Lane", "Male");

        mvc.perform(MockMvcRequestBuilders.post("/customer")
                .content(toJson(testCustomer))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void test4_updateCustomerTest() throws Exception
    {

        testCustomer = new Customer(1003,"Thanos", Long.parseLong("5555555555"), "5 Street Lane", "Male");

        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/customer/1003")
                .content(toJson(testCustomer))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerId").isNumber())
                .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/customer/"+json.get("customerId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.customerId").value((json.get("customerId"))))
                .andExpect(jsonPath("$.customerName").value("Thanos"));

    }

    @Test
    public void test4a_updateCustomerNotFoundTest() throws Exception
    {

        testCustomer = new Customer(0,"Thanos", Long.parseLong("5555555555"), "5 Street Lane", "Male");

        mvc.perform(MockMvcRequestBuilders.put("/customer/0")
                .content(toJson(testCustomer))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


    }

    @Test
    public void test5_deleteCustomerTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/customer/1001")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/customer/1001").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test5a_deleteCustomerNotFoundTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/customer/0")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test6_deleteAllCustomerTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/customer")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/customer").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }


}
