package com.hackerrank.customermicroservice.service;


import com.hackerrank.customermicroservice.exception.BadResourceRequestException;
import com.hackerrank.customermicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.customermicroservice.model.Customer;
import com.hackerrank.customermicroservice.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {


    @Autowired
    private CustomerRepository customerRepo;

    //Method to delete all customer
    @Override
    public void deleteAllCustomers()
    {
        customerRepo.deleteAll();
    }

    //Method to delete a customer by ID
    @Override
    public void deleteCustomerById(Long id)
    {

        if (customerRepo.findById(id).isPresent())
        {
            customerRepo.deleteById(id);

        }
        else
        {
            throw new NoSuchResourceFoundException("Customer Not Found");
        }
    }

    //Method to add a new customer
    @Override
    public Customer createCustomer(Customer customer)
    {
        Customer saveCustomer;

        if (customerRepo.findById(customer.getCustomerId()).isPresent())
        {
            throw new BadResourceRequestException("Customer with same ID exists");
        }

        saveCustomer = customerRepo.save(customer);

        return saveCustomer;
    }

    //Method to fetch a customer by ID
    @Override
    public Customer getCustomerById(Long id)
    {
        Optional<Customer> customer = customerRepo.findById(id);
        Customer returnCustomer;

        if (customer.isPresent())
        {
            returnCustomer = customer.get();
        }
        else
        {
            throw new NoSuchResourceFoundException("Customer does not exist");
        }

        return returnCustomer;
    }

    //Method to fetch all customers
    @Override
    public List<Customer> getAllCustomers()
    {

        List<Customer> custList = new ArrayList<Customer>();

        customerRepo.findAll().forEach(custList::add);

        if (custList.isEmpty())
        {
            throw new NoSuchResourceFoundException("No Customers Found");
        }

        return custList;
    }


    //Update a customer
    @Override
    public Customer updateCustomer(Long id, Customer customer) {

        Customer saveCustomer;

        if (customerRepo.findById(id).isPresent())
        {
            saveCustomer= customerRepo.save(customer);
        }
        else
        {
            throw new NoSuchResourceFoundException("Customer does not exist");
        }

        return saveCustomer;
    }


}
