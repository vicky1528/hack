package com.hackerrank.customermicroservice.service;

import com.hackerrank.customermicroservice.model.Customer;

import java.util.List;

public interface CustomerService {

    //Method to delete all customer
    void deleteAllCustomers();

    //Method to delete a customer by ID
    void deleteCustomerById(Long id);

    //Method to add a new customer
    Customer createCustomer(Customer customer);

    //Method to fetch a customer by ID
    Customer getCustomerById(Long id);

    //Method to fetch all customers
    List<Customer> getAllCustomers();

    //Method to update a customer
    Customer updateCustomer(Long id, Customer customer);
}
