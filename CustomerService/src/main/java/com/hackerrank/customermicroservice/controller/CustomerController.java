package com.hackerrank.customermicroservice.controller;

import com.hackerrank.customermicroservice.exception.BadResourceRequestException;
import com.hackerrank.customermicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.customermicroservice.model.Customer;
import com.hackerrank.customermicroservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    //Get Customer By ID
    @RequestMapping(value="/customer/{id}", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<Customer> getCustomer(@PathVariable(name="id") long id)
    {

        ResponseEntity<Customer> customer = null;

        try
        {
            customer = new ResponseEntity<Customer>(customerService.getCustomerById(id), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            customer = new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            customer = new ResponseEntity<Customer>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return customer;
    }

    //Get all customers
    @RequestMapping(value="/customer", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<Customer>> getAllCustomer()
    {

        ResponseEntity<List<Customer>> response = null;

        try
        {
            response = new ResponseEntity<List<Customer>>(customerService.getAllCustomers(), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            response = new ResponseEntity<List<Customer>>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<List<Customer>>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Create a new customer
    @RequestMapping(value="/customer", method=RequestMethod.POST)
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer)
    {
        ResponseEntity<Customer> response = null;

        try
        {
            response = new ResponseEntity<Customer>(customerService.createCustomer(customer),HttpStatus.CREATED);
        }
        catch (BadResourceRequestException bex)
        {
            response = new ResponseEntity<Customer>(HttpStatus.BAD_REQUEST);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Customer>(HttpStatus.INTERNAL_SERVER_ERROR);
            ex.printStackTrace();

        }

        return response;

    }

    //Update a customer
    @RequestMapping(value="/customer/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(@PathVariable(name="id") long id, @RequestBody Customer customer)
    {
        ResponseEntity<Customer> response = null;

        try
        {
            response = new ResponseEntity<Customer>(customerService.updateCustomer(id, customer),HttpStatus.OK);
        }
        catch (NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Customer>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Delete a customer
    @RequestMapping(value="/customer/{id}", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteCustomer(@PathVariable(name="id") long id)
    {
        ResponseEntity<Void> response = null;

        try
        {
            customerService.deleteCustomerById(id);
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;

    }

    //Delete all customers
    @RequestMapping(value="/customer", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteAllCustomer()
    {
        ResponseEntity<Void> response = null;

        try
        {
            customerService.deleteAllCustomers();
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;

    }



}
