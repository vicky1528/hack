package com.hackerrank.customermicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="customer")
public class Customer implements Serializable{

    public Customer()
    {

    }
    public Customer(long customerId, String customerName, long contactNumber, String address, String gender) {
        super();
        this.customerId = customerId;
        this.customerName = customerName;
        this.contactNumber = contactNumber;
        this.address = address;
        this.gender = gender;
    }

    @Id
    @Column(name="customer_id")
    private long customerId;
    @Column(name="customer_name")
    private String customerName;
    @Column(name="contact_number")
    private long contactNumber;
    @Column(name="address")
    private String address;
    @Column(name="gender")
    private String gender;


    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(long contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }




}
