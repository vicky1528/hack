package com.hackerrank.vendormicroservice.service;

import com.hackerrank.vendormicroservice.model.Vendor;

import java.util.List;

public interface VendorService {

    //Method to delete all Vendor
    void deleteAllVendors();

    //Method to delete a Vendor by ID
    void deleteVendorById(Long id);

    //Method to add a new Vendor
    Vendor createVendor(Vendor vendor);

    //Method to fetch a Vendor by ID
    Vendor getVendorById(Long id);

    //Method to fetch all Vendors
    List<Vendor> getAllVendor();

    //Method to update a Vendor
    Vendor updateVendor(Long id, Vendor vendor);
}
