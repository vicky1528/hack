package com.hackerrank.vendormicroservice.service;

import com.hackerrank.vendormicroservice.exception.BadResourceRequestException;
import com.hackerrank.vendormicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.vendormicroservice.model.Vendor;
import com.hackerrank.vendormicroservice.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("vendorService")
public class VendorServiceImpl implements VendorService{

    @Autowired
    private VendorRepository vendorRepo;

    //Delete All Vendors
    @Override
    public void deleteAllVendors() {

        vendorRepo.deleteAll();

    }

    //Delete Vendor by ID
    @Override
    public void deleteVendorById(Long id) {

        if (vendorRepo.findOne(id) == null)
        {
            throw new NoSuchResourceFoundException("Vendor Not Found");
        }
        else
        {
            vendorRepo.delete(id);
        }

    }


    //Add a new Vendor
    @Override
    public Vendor createVendor(Vendor vendor) {

        Vendor saveVendor;

        if (vendorRepo.findOne(vendor.getVendorId()) != null)
        {
            throw new BadResourceRequestException("Vendor with same ID exists");
        }

        saveVendor = vendorRepo.save(vendor);

        return saveVendor;
    }

    //Get a Vendor by ID
    @Override
    public Vendor getVendorById(Long id) {

        Vendor vendor = vendorRepo.findOne(id);


        if (vendor == null)
        {
            throw new NoSuchResourceFoundException("Vendor does not exist");
        }

        return vendor;

    }

    //Get all Vendors
    @Override
    public List<Vendor> getAllVendor() {

        List<Vendor> vendList = new ArrayList<Vendor>();

        vendorRepo.findAll().forEach(vendList::add);

        if (vendList.isEmpty())
        {
            throw new NoSuchResourceFoundException("No Vendors Found");
        }

        return vendList;
    }

    //Update a Vendor
    @Override
    public Vendor updateVendor(Long id, Vendor vendor) {

        Vendor saveVendor;

        Vendor existVendor = vendorRepo.findOne(id);


        if (existVendor == null)
        {
            throw new NoSuchResourceFoundException("Vendor does not exist");
        }
        else
        {
            saveVendor= vendorRepo.save(vendor);
        }

        return saveVendor;
    }
}
