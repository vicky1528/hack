package com.hackerrank.vendormicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "vendor")
public class Vendor implements Serializable{

    @Id
    @Column(name = "vendor_id")
    private long vendorId;
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "vendor_contact_no")
    private long vendorContactNo;
    @Column(name = "vendor_email")
    private String vendorEmail;
    @Column(name = "vendor_user_name")
    private String vendorUserName;
    @Column(name = "vendor_address")
    private String vendorAddress;

    public Vendor() {
    }

    public Vendor(long vendorId, String vendorName, long vendorContactNo, String vendorEmail, String vendorUserName, String vendorAddress) {
        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.vendorContactNo = vendorContactNo;
        this.vendorEmail = vendorEmail;
        this.vendorUserName = vendorUserName;
        this.vendorAddress = vendorAddress;
    }

    public long getVendorId() {
        return vendorId;
    }

    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public long getVendorContactNo() {
        return vendorContactNo;
    }

    public void setVendorContactNo(long vendorContactNo) {
        this.vendorContactNo = vendorContactNo;
    }

    public String getVendorEmail() {
        return vendorEmail;
    }

    public void setVendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
    }

    public String getVendorUserName() {
        return vendorUserName;
    }

    public void setVendorUserName(String vendorUserName) {
        this.vendorUserName = vendorUserName;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }



}
