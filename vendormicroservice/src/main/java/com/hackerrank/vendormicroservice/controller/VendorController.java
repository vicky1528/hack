package com.hackerrank.vendormicroservice.controller;

import com.hackerrank.vendormicroservice.exception.BadResourceRequestException;
import com.hackerrank.vendormicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.vendormicroservice.model.Vendor;
import com.hackerrank.vendormicroservice.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VendorController {

    @Autowired
    private VendorService vendorService;

    //Get vendor By ID
    @RequestMapping(value="/vendor/{id}", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<Vendor> getVendor(@PathVariable(name="id") long id)
    {

        ResponseEntity<Vendor> response = null;

        try
        {
            response = new ResponseEntity<Vendor>(vendorService.getVendorById(id), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }

    //Get all vendors
    @RequestMapping(value="/vendor", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<Vendor>> getAllVendor()
    {

        ResponseEntity<List<Vendor>> response = null;

        try
        {
            response = new ResponseEntity<List<Vendor>>(vendorService.getAllVendor(), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            response = new ResponseEntity<List<Vendor>>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<List<Vendor>>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Create a new vendor
    @RequestMapping(value="/vendor", method=RequestMethod.POST)
    public ResponseEntity<Vendor> createVendor(@RequestBody Vendor vendor)
    {
        ResponseEntity<Vendor> response = null;

        try
        {
            response = new ResponseEntity<Vendor>(vendorService.createVendor(vendor),HttpStatus.CREATED);
        }
        catch (BadResourceRequestException bex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.BAD_REQUEST);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;

    }

    //Update a vendor
    @RequestMapping(value="/vendor/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Vendor> updateVendor(@PathVariable(name="id") long id, @RequestBody Vendor vendor)
    {
        ResponseEntity<Vendor> response = null;

        try
        {
            response = new ResponseEntity<Vendor>(vendorService.updateVendor(id, vendor),HttpStatus.OK);
        }
        catch (NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Vendor>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Delete a vendor
    @RequestMapping(value="/vendor/{id}", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteVendor(@PathVariable(name="id") long id)
    {
        ResponseEntity<Void> response = null;

        try
        {
            vendorService.deleteVendorById(id);
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;

    }

    //Delete all vendors
    @RequestMapping(value="/vendor", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteAllVendor()
    {
        ResponseEntity<Void> response = null;

        try
        {
            vendorService.deleteAllVendors();
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;

    }

}
