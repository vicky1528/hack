package com.hackerrank.vendormicroservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.vendormicroservice.model.Vendor;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@WebAppConfiguration
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VendorTest {

    @Autowired
    private WebApplicationContext ctx;
    private Vendor testVendor;
    private MockMvc mvc;

    @Before
    public void setup()
    {
        mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void test1_getVendorTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/vendor/1001").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void test1a_getVendorInvalidTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/vendor/0").accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }

    @Test
    public void test2_getAllVendorTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/vendor").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$[0].vendorId").isNumber())
                .andExpect(jsonPath("$[0].vendorName").isString())
                .andExpect(jsonPath("$[0].vendorContactNo").isNumber())
                .andExpect(jsonPath("$[0].vendorEmail").isString())
                .andExpect(jsonPath("$[0].vendorUserName").isString())
                .andExpect(jsonPath("$[0].vendorAddress").isString());
    }

    @Test
    public void test3_createVendorTest() throws Exception
    {

        testVendor = new Vendor(1004,"Aquaman", Long.parseLong("5555555555"), "test6@mail.com", "aqua.man","6 Street Lane");

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/vendor")
                .content(toJson(testVendor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.vendorId").isNumber())
                .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/vendor/"+json.get("vendorId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.vendorId").value((json.get("vendorId"))))
                .andExpect(jsonPath("$.vendorName").value("Aquaman"));

    }

    @Test
    public void test3a_createVendorBadRequestTest() throws Exception
    {

        testVendor = new Vendor(1002,"Aquaman", Long.parseLong("5555555555"), "test6@mail.com", "aqua.man","6 Street Lane");

        mvc.perform(MockMvcRequestBuilders.post("/vendor")
                .content(toJson(testVendor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void test4_updateVendorTest() throws Exception
    {

        testVendor = new Vendor(1003,"Aquaman", Long.parseLong("5555555555"), "test6@mail.com", "aqua.man","6 Street Lane");

        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/vendor/1003")
                .content(toJson(testVendor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.vendorId").isNumber())
                .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/vendor/"+json.get("vendorId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.vendorId").value((json.get("vendorId"))))
                .andExpect(jsonPath("$.vendorName").value("Aquaman"));

    }

    @Test
    public void test4a_updateVendorNotFoundTest() throws Exception
    {

        testVendor = new Vendor(0,"Aquaman", Long.parseLong("5555555555"), "test6@mail.com", "aqua.man","6 Street Lane");

        mvc.perform(MockMvcRequestBuilders.put("/vendor/0")
                .content(toJson(testVendor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test5_deleteVendorTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/vendor/1001")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/vendor/1001").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test5a_deleteVendorNotFoundTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/vendor/0")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


    }

    @Test
    public void test6_deleteAllVendorTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/vendor")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/vendor").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }



}
