create table orders(order_id bigint, customer_id bigint, payment_channel varchar(255), is_Cod char(1), order_status varchar(255), order_created_on bigint, total_amt bigint, shipping_address varchar(255), primary key(order_id));
create table orderlineitem(order_line_item_id bigint, sku_id bigint, order_id bigint, itm_qty int, primary key(order_line_item_id));

insert into orders(order_id,customer_id,payment_channel,is_Cod,order_status,order_created_on,total_amt,shipping_address) values ('400001','100001','Credit Card','N','Received','0','150','1 Street drive');
insert into orders(order_id,customer_id,payment_channel,is_Cod,order_status,order_created_on,total_amt,shipping_address) values ('400002','100001','Credit Card','N','Received','0','250','1 Street drive');
insert into orders(order_id,customer_id,payment_channel,is_Cod,order_status,order_created_on,total_amt,shipping_address) values ('400003','100001','Credit Card','N','Received','0','50','1 Street drive');
insert into orders(order_id,customer_id,payment_channel,is_Cod,order_status,order_created_on,total_amt,shipping_address) values ('400004','100002','Debit Card','N','Received','0','150','2 Street drive');

insert into orderlineitem(order_line_item_id, sku_id, order_id, itm_qty) values ('500001','300001','400001','2');
insert into orderlineitem(order_line_item_id, sku_id, order_id, itm_qty) values ('500002','300002','400001','1');
insert into orderlineitem(order_line_item_id, sku_id, order_id, itm_qty) values ('500003','300001','400002','1');