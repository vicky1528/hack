package com.hackerrank.ordermicroservice.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.ordermicroservice.dto.OrderQDto;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class QueueProducer {

    @Value("${fanout.exchange}")
    private String fanoutExchange;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public QueueProducer(RabbitTemplate rabbitTemplate) {
        super();
        this.rabbitTemplate = rabbitTemplate;
    }

    public void produce(OrderQDto orderQDto) throws Exception {
        System.out.println("Inside produce");
        rabbitTemplate.setExchange(fanoutExchange);
        rabbitTemplate.convertAndSend(new ObjectMapper().writeValueAsString(orderQDto));
        //rabbitTemplate.convertAndSend(orderQDto);
        System.out.println("Sent to Q");
    }
}

