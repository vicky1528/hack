package com.hackerrank.ordermicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "orders")
public class Order implements Serializable {

    @Id
    @Column(name = "order_id")
    private long orderId;
    @Column(name = "customer_Id")
    private long customerId;
    @Column(name = "payment_channel")
    private String paymentChannel;
    @Column(name = "is_Cod")
    private String isCod;
    @Column(name = "order_status")
    private String orderStatus;
    @Column(name = "order_created_on")
    private Long orderCreatedOn;
    @Column(name = "total_amt")
    private double totalAmt;
    @Column(name = "shipping_address")
    private String shippingAddress;
    public Order() {

    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderCreatedOn() {
        return orderCreatedOn;
    }

    public void setOrderCreatedOn(Long orderCreatedOn) {
        this.orderCreatedOn = orderCreatedOn;
    }

    public double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getIsCod() {
        return isCod;
    }

    public void setIsCod(String isCod) {
        this.isCod = isCod;
    }
}
