package com.hackerrank.ordermicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="orderlineitem")
public class OrderLineItem implements Serializable {

    @Id
    @Column(name="order_line_item_id")
    private long orderLineItemId;
    @Column(name="sku_id")
    private long skuId;
    @Column(name="order_id")
    private long orderId;
    @Column(name="itm_qty")
    private int itmQty;

    public long getOrderLineItemId() {
        return orderLineItemId;
    }

    public void setOrderLineItemId(long orderLineItemId) {
        this.orderLineItemId = orderLineItemId;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getItmQty() {
        return itmQty;
    }

    public void setItmQty(int itmQty) {
        this.itmQty = itmQty;
    }
}
