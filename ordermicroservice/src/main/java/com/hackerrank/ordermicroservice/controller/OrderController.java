package com.hackerrank.ordermicroservice.controller;

import com.hackerrank.ordermicroservice.dto.OrderDto;
import com.hackerrank.ordermicroservice.exception.BadResourceRequestException;
import com.hackerrank.ordermicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.ordermicroservice.model.Order;
import com.hackerrank.ordermicroservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    //Get Order By Customer ID
    @RequestMapping(value="/order/cust/{custId}", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<OrderDto>> getCustomerOrders(@PathVariable(name="custId") long id)
    {

        ResponseEntity<List<OrderDto>> customerOrder = null;

        try
        {
            customerOrder = new ResponseEntity<List<OrderDto>>(orderService.getAllOrdersByCustomer(id), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nex)
        {
            customerOrder = new ResponseEntity<List<OrderDto>>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            customerOrder = new ResponseEntity<List<OrderDto>>(HttpStatus.INTERNAL_SERVER_ERROR);
            ex.printStackTrace();

        }

        return customerOrder;
    }

    //Get Order By ID
    @RequestMapping(value="/order/{Id}", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<OrderDto> getOrderById(@PathVariable(name="Id") long id)
    {

        ResponseEntity<OrderDto> orderDto = null;

        try
        {
            orderDto = new ResponseEntity<OrderDto>(orderService.getOrderById(id), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nex)
        {
            orderDto = new ResponseEntity<OrderDto>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            orderDto = new ResponseEntity<OrderDto>(HttpStatus.INTERNAL_SERVER_ERROR);
            ex.printStackTrace();

        }

        return orderDto;
    }

    //Add a new Order
    @RequestMapping(value="/order", method= RequestMethod.POST)
    public @ResponseBody ResponseEntity<OrderDto> addNewOrder(@RequestBody OrderDto orderDto)
    {
        ResponseEntity<OrderDto> response = null;

        try
        {
            response = new ResponseEntity<OrderDto>(orderService.AddNewOrder(orderDto),HttpStatus.CREATED);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<OrderDto>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    //Delete an Order
    @RequestMapping(value="/order/{Id}", method= RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<String> addNewOrder(@PathVariable(name="Id") long id)
    {
        ResponseEntity<String> response = null;

        try
        {
            response = new ResponseEntity<String>("Order Deleted",HttpStatus.OK);
        }
        catch (NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        catch (Exception ex)
        {
            response = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    //Update the status of an Order
    @RequestMapping(value="/order/{Id}", method= RequestMethod.PATCH)
    public @ResponseBody ResponseEntity<OrderDto> addNewOrder(@PathVariable(name="Id") long id, @RequestBody OrderDto orderDto)
    {
        ResponseEntity<OrderDto> response = null;

        try
        {
            response = new ResponseEntity<OrderDto>(orderService.updateOrderStatus(id,orderDto),HttpStatus.OK);
        }
        catch (NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<OrderDto>(HttpStatus.NOT_FOUND);
        }
        catch (Exception ex)
        {
            response = new ResponseEntity<OrderDto>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }


}
