package com.hackerrank.ordermicroservice.dto;

import com.hackerrank.ordermicroservice.model.Order;
import com.hackerrank.ordermicroservice.model.OrderLineItem;

import java.util.List;

public class OrderDto {

    private Order orderEntry;
    private List<OrderLineItem> orderLineItemlist;
    /*
   // private long orderId;
   // private long customerId;
   // private String paymentChannel;
   // private boolean isCod;
   // private String orderStatus;
   // private Long orderCreatedOn;
   // private double totalAmt;
   // private String shippingAddress;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public boolean isCod() {
        return isCod;
    }

    public void setCod(boolean cod) {
        isCod = cod;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderCreatedOn() {
        return orderCreatedOn;
    }

    public void setOrderCreatedOn(Long orderCreatedOn) {
        this.orderCreatedOn = orderCreatedOn;
    }

    public double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
       */
    public List<OrderLineItem> getOrderLineItemlist() {
        return orderLineItemlist;
    }


    public Order getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(Order orderEntry) {
        this.orderEntry = orderEntry;
    }

    public void setOrderLineItemlist(List<OrderLineItem> orderLineItemlist) {
        this.orderLineItemlist = orderLineItemlist;
    }
}
