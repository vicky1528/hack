package com.hackerrank.ordermicroservice.repository;

import com.hackerrank.ordermicroservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface OrderRepository extends JpaRepository<Order,Long> {

    @Query(value = "SELECT * FROM Orders ord WHERE ord.customer_id = (:custId)", nativeQuery=true)
    public List<Order> findByCustomerId(@Param("custId") Long custId);
}
