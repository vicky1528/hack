package com.hackerrank.ordermicroservice.service;

import com.hackerrank.ordermicroservice.dto.OrderDto;
import com.hackerrank.ordermicroservice.model.Order;

import java.util.List;

public interface OrderService {

    //Get all orders of customers
    List<OrderDto> getAllOrdersByCustomer(Long customerId);

    //Add new order
    OrderDto AddNewOrder(OrderDto orderDto);

    //Get Order by Order ID
    OrderDto getOrderById(Long orderId);

    //Update Order Status
    OrderDto updateOrderStatus(Long id, OrderDto orderDto);

    //Delete Order by ID
    void deleteOrderById(Long orderId);



}
