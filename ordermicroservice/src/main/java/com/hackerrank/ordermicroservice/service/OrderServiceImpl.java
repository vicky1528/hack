package com.hackerrank.ordermicroservice.service;


import com.hackerrank.ordermicroservice.dto.OrderDto;
import com.hackerrank.ordermicroservice.dto.OrderQDto;
import com.hackerrank.ordermicroservice.exception.BadResourceRequestException;
import com.hackerrank.ordermicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.ordermicroservice.model.Order;
import com.hackerrank.ordermicroservice.model.OrderLineItem;
import com.hackerrank.ordermicroservice.repository.OrderLineItemRepository;
import com.hackerrank.ordermicroservice.repository.OrderRepository;
import com.hackerrank.ordermicroservice.util.QueueProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("orderservice")
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderRepository orderRepo;

    @Autowired
    private OrderLineItemRepository orderLineItemRepo;

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private QueueProducer queueProducer;


    //Method to get all orders by customer id
    @Override
    public List<OrderDto> getAllOrdersByCustomer(Long customerId) {

        List<Order> custOrders = new ArrayList<Order>();
        List<OrderDto> customerOrder = new ArrayList<OrderDto>();

        orderRepo.findByCustomerId(customerId).forEach(custOrders::add);

        if (custOrders.isEmpty())
        {
            throw new NoSuchResourceFoundException("No Orders for the Customer");
        }

        for(Order order:custOrders)
        {
            List<OrderLineItem> orderItems = new ArrayList<OrderLineItem>();
            OrderDto orderDto = new OrderDto();
            orderDto.setOrderEntry(order);
            orderItems = orderLineItemRepo.findByOrderId(order.getOrderId());
            orderDto.setOrderLineItemlist(orderItems);
            customerOrder.add(orderDto);

        }

       // customerOrder = allOrders.stream().filter(o -> o.getCustomerId() == customerId).collect(Collectors.toList());

        return customerOrder;

    }

    @Override
    public OrderDto AddNewOrder(OrderDto orderDto) {

        Order order = new Order();


        order = orderDto.getOrderEntry();
        List<OrderLineItem> orderItemsList = orderDto.getOrderLineItemlist();


        orderRepo.save(order);



        for (OrderLineItem orderItem:orderItemsList)
        {
            System.out.println("Inside Add" + orderItem.getOrderLineItemId());
            orderLineItemRepo.save(orderItem);
            OrderQDto orderQDto = new OrderQDto();
            orderQDto.setSkuId(orderItem.getSkuId());
            orderQDto.setQty(orderItem.getItmQty());
            try
            {
                queueProducer.produce(orderQDto);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }


        }

        try
        {
            sendEmail(order.getOrderId());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return orderDto;

    }

    @Override
    public OrderDto getOrderById(Long orderId) {

        OrderDto orderDto = new OrderDto();
        Order order;

        if (orderRepo.findById(orderId).isPresent())
        {
            order = orderRepo.findById(orderId).get();
            List<OrderLineItem> orderItems = orderLineItemRepo.findByOrderId(orderId);
            orderDto.setOrderEntry(order);
            orderDto.setOrderLineItemlist(orderItems);
        }
        else
        {
            throw new NoSuchResourceFoundException("Order Id Not Found");
        }

        return orderDto;


    }

    @Override
    public OrderDto updateOrderStatus(Long id, OrderDto orderDto) {

        Order order = orderDto.getOrderEntry();
        List<OrderLineItem> orderItems = orderDto.getOrderLineItemlist();
        OrderDto response = new OrderDto();
        response.setOrderLineItemlist(orderItems);
        response.setOrderEntry(order);

        if (!orderRepo.findById(order.getOrderId()).isPresent())
        {
            throw new NoSuchResourceFoundException("Order Not Found");
        }
        else
        {
            orderRepo.save(order);
            for (OrderLineItem ordeItem:orderItems)
            {
                if (!orderLineItemRepo.findById(ordeItem.getOrderLineItemId()).isPresent())
                {
                    throw new NoSuchResourceFoundException("Order Line Item Not Found");
                }
                else
                {
                    orderLineItemRepo.save(ordeItem);
                }
            }
        }

        return response;

    }

    @Override
    public void deleteOrderById(Long orderId) {


        List<OrderLineItem> orderItems = new ArrayList<OrderLineItem>();

        if (!orderRepo.findById(orderId).isPresent())
        {
            throw new NoSuchResourceFoundException("Order Not Found");
        }
        else
        {
            orderRepo.deleteById(orderId);
            orderItems = orderLineItemRepo.findByOrderId(orderId);

            for (OrderLineItem orderItem:orderItems)
            {
                if (!orderLineItemRepo.findById(orderItem.getOrderLineItemId()).isPresent())
                {
                    throw new NoSuchResourceFoundException("Order Item Not Found");
                }
                else
                {
                    orderLineItemRepo.deleteById(orderItem.getOrderLineItemId());
                }
            }
        }

    }

    private void sendEmail(Long id) throws Exception
    {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo("vigneshraj123@gmail.com");
        helper.setSubject("Order  " + id + " Received");
        helper.setText("We have received your order and we are working on it");

        sender.send(message);
    }
}
