package com.hackerrank.inventorymicroservice.config;


import com.hackerrank.inventorymicroservice.model.Inventory;
import com.hackerrank.inventorymicroservice.repository.InventoryRepository;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    public JobBuilderFactory jobs;

    @Autowired
    public StepBuilderFactory steps;

    @Autowired
    private InventoryRepository inventoryRepo;

    @Bean
    public FlatFileItemReader<Inventory> reader()
    {

        return new FlatFileItemReaderBuilder<Inventory>()
                .name("itemReader")
                .resource(new ClassPathResource("inventory.csv"))
                .delimited()
                .delimiter(";")
                .names(new String[]{"skuId", "productName", "productLabel", "inventoryOnHand", "minQtyReq", "price"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Inventory>()
                {{
                    setTargetType(Inventory.class);
                }}).build();


    }

    @Bean
    public RepositoryItemWriter<Inventory> writer()
    {
        return new RepositoryItemWriterBuilder<Inventory>().methodName("save").repository(inventoryRepo).build();
    }

    @Bean
    public Job inventoryJob(JobExecutionListener listener)
    {
        return jobs.get("inventoryJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1())
                .end()
                .build();

    }

    @Bean
    public Step step1()
    {
        return steps.get("step1")
                .<Inventory, Inventory>chunk(1)
                .reader(reader())
                .writer(writer())
                .build();
    }

    @Bean
    public JobExecutionListener listener()
    {
        return new JobExecutionListener() {
            @Override
            public void beforeJob(JobExecution jobExecution) {

            }

            @Override
            public void afterJob(JobExecution jobExecution) {

                if(jobExecution.getStatus() == BatchStatus.COMPLETED)
                {
                    System.out.println("Job Finished");
                    inventoryRepo.findAll().forEach(inventory -> System.out.println(inventory.getProductName()));
                }

            }
        };
    }






}
