package com.hackerrank.inventorymicroservice.config;

import com.hackerrank.inventorymicroservice.repository.InventoryRepository;
import com.hackerrank.inventorymicroservice.util.InventoryTaskLet;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Configuration
@EnableBatchProcessing
public class ScheduleBatchJob {


    @Autowired
    public JobBuilderFactory jobs;

    @Autowired
    public StepBuilderFactory steps;

    @Autowired
    private InventoryRepository inventoryRepo;


    @Bean
    public Job schInventoryJob()
    {
        return jobs.get("schInventoryJob")
                .incrementer(new RunIdIncrementer())
                .start(step2())
                .build();

    }

    @Bean
    public Step step2()
    {
        return steps.get("step2")
                .tasklet(inventoryTaskLet())
                .build();
    }

    @Scheduled(cron = "*/5 * * * * ")
    public void perform() throws Exception
    {
        System.out.println("Job Started at :" + new Date());
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();

        JobParameters param = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();

        JobExecution execution = jobLauncher.run(schInventoryJob(), param);

        System.out.println("Job finished with status :" + execution.getStatus());
    }

    @Bean
    public InventoryTaskLet inventoryTaskLet()
    {
        InventoryTaskLet taskLet = new InventoryTaskLet();
        taskLet.setInventoryRepo(inventoryRepo);
        return taskLet;
    }

    @Bean
    public JobExecutionListener listener()
    {
        return new JobExecutionListener() {
            @Override
            public void beforeJob(JobExecution jobExecution) {

            }

            @Override
            public void afterJob(JobExecution jobExecution) {

                if(jobExecution.getStatus() == BatchStatus.COMPLETED)
                {
                    System.out.println("Job Finished");
                    inventoryRepo.findAll().forEach(inventory -> System.out.println(inventory.getProductName()));
                }

            }
        };
    }


}
