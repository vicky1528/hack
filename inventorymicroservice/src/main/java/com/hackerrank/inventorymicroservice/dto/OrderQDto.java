package com.hackerrank.inventorymicroservice.dto;

public class OrderQDto {

    Long skuId;
    int qty;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
