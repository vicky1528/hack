package com.hackerrank.inventorymicroservice.controller;

import com.hackerrank.inventorymicroservice.exception.BadResourceRequestException;
import com.hackerrank.inventorymicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.inventorymicroservice.model.Inventory;
import com.hackerrank.inventorymicroservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    //Get Item By ID
    @RequestMapping(value="/item/{id}", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<Inventory> getInventory(@PathVariable(name="id") long id)
    {

        ResponseEntity<Inventory> response = null;

        try
        {
            response = new ResponseEntity<Inventory>(inventoryService.getItemById(id), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.INTERNAL_SERVER_ERROR);
            ex.printStackTrace();

        }

        return response;
    }

    //Get all Items
    @RequestMapping(value="/item", method= RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<Inventory>> getAllItems()
    {

        ResponseEntity<List<Inventory>> response = null;

        try
        {
            response = new ResponseEntity<List<Inventory>>(inventoryService.getAllItem(), HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nrex)
        {
            response = new ResponseEntity<List<Inventory>>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<List<Inventory>>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Create a new item
    @RequestMapping(value="/item", method=RequestMethod.POST)
    public ResponseEntity<Inventory> createInventory(@RequestBody Inventory inventory)
    {
        ResponseEntity<Inventory> response = null;

        try
        {
            response = new ResponseEntity<Inventory>(inventoryService.createItem(inventory),HttpStatus.CREATED);
        }
        catch (BadResourceRequestException bex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.BAD_REQUEST);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;

    }

    //Update a Item
    @RequestMapping(value="/item/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Inventory> updateInventory(@PathVariable(name="id") long id, @RequestBody Inventory inventory)
    {
        ResponseEntity<Inventory> response = null;

        try
        {
            response = new ResponseEntity<Inventory>(inventoryService.updateItem(id, inventory),HttpStatus.OK);
        }
        catch (NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Inventory>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    //Delete a Item
    @RequestMapping(value="/item/{id}", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteInventory(@PathVariable(name="id") long id)
    {
        ResponseEntity<Void> response = null;

        try
        {
            inventoryService.deleteItemById(id);
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(NoSuchResourceFoundException nex)
        {
            response = new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;

    }

    //Delete all items
    @RequestMapping(value="/item", method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Void> deleteAllInventory()
    {
        ResponseEntity<Void> response = null;

        try
        {
            inventoryService.deleteAllItems();
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
            System.out.println("Delete Inventory " + ex);
        }

        return response;

    }


    //Update the inventory quantity on a booking
    @RequestMapping(value="/item/{id}", method=RequestMethod.PATCH)
    public @ResponseBody ResponseEntity<Void> updateItemQuantity(@PathVariable(name="id") long id, @RequestBody Inventory inventory)
    {
        ResponseEntity<Void> response = null;

        try
        {
            inventoryService.deleteAllItems();
            response = new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(Exception ex)
        {
            response = new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
            System.out.println("Delete Inventory " + ex);
        }

        return response;

    }


}
