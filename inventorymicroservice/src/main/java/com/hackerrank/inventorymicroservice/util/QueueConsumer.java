package com.hackerrank.inventorymicroservice.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.inventorymicroservice.dto.OrderQDto;
import com.hackerrank.inventorymicroservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumer {

    @Autowired
    InventoryService inventoryService;

    public void receiveMessage(String message) {
        System.out.println("Received (String) " + message);
        processMessage(message);
    }

    public void receiveMessage(byte[] message) {
        String strMessage = new String(message);
        System.out.println("Received (No String) " + strMessage);
        processMessage(strMessage);
    }

    private void processMessage(String message) {
        try {
            OrderQDto orderQDto = new ObjectMapper().readValue(message, OrderQDto.class);
            inventoryService.updateItemQuantity(orderQDto);

        } catch (JsonParseException e) {
            System.out.println("Bad JSON in message: " + message);
        } catch (JsonMappingException e) {
            System.out.println("cannot map JSON to NotificationRequest: " + message);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
