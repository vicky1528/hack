package com.hackerrank.inventorymicroservice.util;


import com.hackerrank.inventorymicroservice.model.Inventory;
import com.hackerrank.inventorymicroservice.repository.InventoryRepository;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;


import java.util.ArrayList;
import java.util.List;


public class InventoryTaskLet implements Tasklet {

    private InventoryRepository inventoryRepo;


    public InventoryRepository getInventoryRepo() {
        return inventoryRepo;
    }

    public void setInventoryRepo(InventoryRepository inventoryRepo) {
        this.inventoryRepo = inventoryRepo;
    }




    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext context)
    {
        List<Inventory> listInventory = new ArrayList<Inventory>();

        //System.out.println("Inside Tasklet " + inventoryRepo.count());
        inventoryRepo.findAll().forEach(listInventory::add);

        for(Inventory inventory:listInventory)
        {
            if (inventory.getInventoryOnHand() < inventory.getMinQtyReq())
            {
                System.out.println("Inside the right place");
                inventory.setInventoryOnHand(inventory.getInventoryOnHand() + 20);
                inventoryRepo.save(inventory);

            }
        }

        return RepeatStatus.FINISHED;
    }

}
