package com.hackerrank.inventorymicroservice.service;

import com.hackerrank.inventorymicroservice.dto.OrderQDto;
import com.hackerrank.inventorymicroservice.exception.BadResourceRequestException;
import com.hackerrank.inventorymicroservice.exception.NoSuchResourceFoundException;
import com.hackerrank.inventorymicroservice.model.Inventory;
import com.hackerrank.inventorymicroservice.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService{

    @Autowired
    private InventoryRepository inventoryRepo;

    //Delete all Items
    @Override
    public void deleteAllItems() {
        inventoryRepo.deleteAll();
    }

    //Delete Item by ID
    @Override
    public void deleteItemById(Long id) {

        if (inventoryRepo.findById(id).isPresent())
        {
            inventoryRepo.deleteById(id);

        }
        else
        {
            throw new NoSuchResourceFoundException("Item Not Found");
        }

    }

    //Add a new Item
    @Override
    public Inventory createItem(Inventory inventory) {

        Inventory saveItem;

        if (inventoryRepo.findById(inventory.getSkuId()).isPresent())
        {
            throw new BadResourceRequestException("Item with same ID exists");
        }

        saveItem = inventoryRepo.save(inventory);

        return saveItem;
    }

    //Get an Item by ID
    @Override
    public Inventory getItemById(Long id) {


        Optional<Inventory> optItem = inventoryRepo.findById(id);
        Inventory item = null;


        if (optItem.isPresent())
        {
            item=optItem.get();
        }
        else
        {
            throw new NoSuchResourceFoundException("Item does not exist");
        }

        return item;
    }

    //Get all Items
    @Override
    public List<Inventory> getAllItem() {

        List<Inventory> itemList = new ArrayList<Inventory>();

        inventoryRepo.findAll().forEach(itemList::add);

        if (itemList.isEmpty())
        {
            throw new NoSuchResourceFoundException("No Items Found");
        }

        return itemList;
    }

    //Update an Item
    @Override
    public Inventory updateItem(Long id, Inventory inventory) {

        Inventory saveItem;




        if ((inventoryRepo.findById(id)).isPresent())
        {
            saveItem= inventoryRepo.save(inventory);
        }
        else
        {
            throw new NoSuchResourceFoundException("Item does not exist");

        }

        return saveItem;
    }


    @Override
    public void updateItemQuantity(OrderQDto orderQDto)
    {
        System.out.println("Inside update item quantity");

        Inventory inventory = inventoryRepo.findById(orderQDto.getSkuId()).get();
        inventory.setInventoryOnHand(inventory.getInventoryOnHand() - orderQDto.getQty());
        System.out.println(inventory.getInventoryOnHand());
        inventoryRepo.save(inventory);
    }
}
