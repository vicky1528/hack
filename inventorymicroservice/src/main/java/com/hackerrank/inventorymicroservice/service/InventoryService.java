package com.hackerrank.inventorymicroservice.service;

import com.hackerrank.inventorymicroservice.dto.OrderQDto;
import com.hackerrank.inventorymicroservice.model.Inventory;

import java.util.List;


public interface InventoryService {

    //Method to delete all Items
    void deleteAllItems();

    //Method to delete a Item by ID
    void deleteItemById(Long id);

    //Method to add a new Item
    Inventory createItem(Inventory inventory);

    //Method to fetch a Item by ID
    Inventory getItemById(Long id);

    //Method to fetch all Item
    List<Inventory> getAllItem();

    //Method to update a Item
    Inventory updateItem(Long id, Inventory inventory);

    //Method to update the quantity of the item
    void updateItemQuantity(OrderQDto orderQDto);

}
