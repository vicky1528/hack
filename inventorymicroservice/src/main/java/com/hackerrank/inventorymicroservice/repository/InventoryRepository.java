package com.hackerrank.inventorymicroservice.repository;

import com.hackerrank.inventorymicroservice.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {
}
