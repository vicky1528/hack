package com.hackerrank.inventorymicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.io.Serializable;

@Entity
@Table(name="inventory")
public class Inventory implements Serializable{

    @Id
    @Column(name = "sku_id")
    private long skuId;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_label")
    private String productLabel;
    @Column(name = "inventory_onhand")
    private int inventoryOnHand;
    @Column(name = "min_qty_req")
    private int minQtyReq;
    @Column(name = "price")
    private double price;

    public Inventory() {
    }

    public Inventory(long skuId, String productName, String productLabel, int inventoryOnHand, int minQtyReq, double price) {
        this.skuId = skuId;
        this.productName = productName;
        this.productLabel = productLabel;
        this.inventoryOnHand = inventoryOnHand;
        this.minQtyReq = minQtyReq;
        this.price = price;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public int getInventoryOnHand() {
        return inventoryOnHand;
    }

    public void setInventoryOnHand(int inventoryOnHand) {
        this.inventoryOnHand = inventoryOnHand;
    }

    public int getMinQtyReq() {
        return minQtyReq;
    }

    public void setMinQtyReq(int minQtyReq) {
        this.minQtyReq = minQtyReq;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }





}
