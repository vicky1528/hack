package com.hackerrank.inventorymicroservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackerrank.inventorymicroservice.model.Inventory;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InventoryTest {

    @Autowired
    private WebApplicationContext ctx;
    private Inventory testInventory;
    private MockMvc mvc;

    @Before
    public void setup()
    {
        mvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void test1_getInventoryTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/item/200002").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void test1a_getInventoryInvalidTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/item/0").accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }

    @Test
    public void test2_getAllInventoryTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.get("/item").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$[0].skuId").isNumber())
                .andExpect(jsonPath("$[0].productName").isString())
                .andExpect(jsonPath("$[0].productLabel").isString())
                .andExpect(jsonPath("$[0].inventoryOnHand").isNumber())
                .andExpect(jsonPath("$[0].minQtyReq").isNumber())
                .andExpect(jsonPath("$[0].price").isNumber());
    }

    @Test
    public void test3_createInventoryTest() throws Exception
    {

        testInventory = new Inventory(10005,"Toy Car", "Toy",25, 10, 50);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/item")
                .content(toJson(testInventory))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.skuId").isNumber())
                .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/item/"+json.get("skuId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.skuId").value((json.get("skuId"))))
                .andExpect(jsonPath("$.productName").value("Toy Car"));

    }

    @Test
    public void test3a_createInventoryBadRequestTest() throws Exception
    {

        testInventory = new Inventory(200001,"Toy Car", "Toy",25, 10, 50);

        mvc.perform(MockMvcRequestBuilders.post("/item")
                .content(toJson(testInventory))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void test4_updateInventoryTest() throws Exception
    {

        testInventory = new Inventory(200001,"Laptop", "Samsung",25, 10, 50);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.put("/item/200001")
                .content(toJson(testInventory))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.skuId").isNumber())
                .andReturn();

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        mvc.perform(MockMvcRequestBuilders.get("/item/"+json.get("skuId")).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.skuId").value((json.get("skuId"))))
                .andExpect(jsonPath("$.productName").value("Laptop"));

    }


    @Test
    public void test4a_updateInventoryNotFoundTest() throws Exception
    {

        testInventory = new Inventory(0,"Toy Car", "Toy",25, 10, 50);

        mvc.perform(MockMvcRequestBuilders.put("/item/0")
                .content(toJson(testInventory))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test5_deleteInventoryTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/item/200001")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/item/200001").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test5a_deleteInventoryNotFoundTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/item/200008")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void test6_deleteAllInventoryTest() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.delete("/item")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform(MockMvcRequestBuilders.get("/item").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }

}
